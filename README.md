<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 11-06-2024.

<!-- Update the below line with your artifact name -->
# Junos File Transfer

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [FAQ](#faq)
* [Additional Information](#additional-information)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->
This Pre-Built Automation contains a workflow designed to perform file transfer for Junos OS devices orchestrated by NSO.
The workflow utilizes the RPC command to execute shell commands such as SSH and SCP on the Junos device.

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 10 min. (depends on file size and network speed)

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2023.1`
* NSO adapter

## Requirements

This Pre-Built Automation requires the following:

<!-- Unordered list highlighting the requirements of the artifact -->
<!-- EXAMPLE -->
* Junos OS device
* NSO (with Juno NED)
* SSH Public Key Authentication for device to remote server access

## Features

The main benefits and features of the Pre-Built Automation are outlined below.

<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
* Utilize shell commands on the Juno device to perform MD5 and SCP commands
* Validate sufficient storage space before download
* Validate file integrity after download
* Allow zero-touch mode of operation
* Utilizes SSH public key authentication (to remote server) for enhanced security
* Exposes runtime outcome (as a job variable) to parent job 

<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements

<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->
Add support for Junos Os devices managed by IAG (Ansible)

## How to Install

To install the Pre-Built Automation:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built Automation. 
* The Pre-Built Automation can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built Automation:
Navigate to the Operations Manager app in IAP, then select to run the JunOS File Transfer Pre-Built.
Fill up the form with the following details:

* Zero Touch (checkbox) - eliminate user interactions
* Perform MD5 check (checkbox) - validate file integrity (using MD5) after download
* Device Name - NSO Junos OS device name to perform SCP on 
* Local Path - local device file system path to save file (Ex. `/var/tmp/`)
* Username - remote file server username to be used with SSH and SCP (Ex. `user1`)
* IP Address - remote file server IP address (Ex. `192.168.240.25`)
* Path - remote file server path to file (Ex. `/var/opt/`)
* File Name - remote file server file to be downloaded (Ex. `foo.bar`)
<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->

## FAQ

### MD5 verification fails due to copy command timeout
The copy command may timeout when trying to copy large files from the remote server. The following error can appear in NSO logs:
```
{
  "jsonrpc": "2.0",
  "error": {
    "type": "rpc.method.failed",
    "code": -32000,
    "message": "Method failed",
    "data": {
      "reason": "DEVICE_NAME: transport timeout; closing session"
    },
    "internal": "jsonrpc_schema2181"
}
```
In order to fix this issue, make sure to give device read-timeout sufficient time to perform the copy command:
```
admin@ncs% set devices device DEVICE_NAME read-timeout 400
admin@ncs% comm
Commit complete.
```
## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built Automation.
