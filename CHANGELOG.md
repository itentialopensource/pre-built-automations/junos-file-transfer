
## 0.0.8 [11-06-2024]

Adds deprecation notice

See merge request itentialopensource/pre-built-automations/junos-file-transfer!10

2024-11-06 21:20:39 +0000

---

## 0.0.7 [07-11-2024]

Removes images and updates IAP version in README

See merge request itentialopensource/pre-built-automations/junos-file-transfer!7

2024-07-11 16:24:39 +0000

---

## 0.0.6 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/junos-file-transfer!6

---

## 0.0.5 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/junos-file-transfer!5

---

## 0.0.4 [12-16-2021]

* Update to 2021.2

See merge request itentialopensource/pre-built-automations/junos-file-transfer!4

---

## 0.0.3 [07-03-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/junos-file-transfer!3

---

## 0.0.2 [03-19-2021]

* Patch/lb 515 master

See merge request itentialopensource/pre-built-automations/junos-file-transfer!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n\n
